A fork of https://github.com/apache/maven-surefire to deploy yet unreleased 3.0.0-M6 to
https://gnss-maven-artifactory-prod.s3-ap-southeast-2.amazonaws.com.

Some useful commands

```bash
# update version numbers
find . -name "pom.xml" | xargs -d '\n' sed -i 's/3.0.0-M6-SNAPSHOT/3.0.0-M6-ga-1/g'

# deploy
export AWS_ACCESS_KEY_ID=...
export AWS_SECRET_ACCESS_KEY=...
eval $(assume-role-maven-artifactory-writer.sh prod)
mvn deploy -Dmaven.test.skip -Drat.skip -DpublicRepository
```
