{ sources ? import ./sources.nix }:

import sources.nixpkgs {
  overlays = [
    (pkgs: old: {
      gnss-common = import sources.gnss-common { inherit pkgs; };
    })
  ];
}
