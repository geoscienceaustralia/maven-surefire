let
  pkgs = import ./nix {};

  buildTools = with pkgs; [
    gnss-common.assumeRoleMavenArtifactoryWriter
    jdk11
    (maven.override { jdk = jdk11; })
  ];

  devEnv = with pkgs; buildEnv {
    name = "devEnv";
    paths = buildTools;
  };
in
  pkgs.mkShell {
    buildInputs = [
      devEnv
    ];
  }
